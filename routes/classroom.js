var express = require("express");
var bodyParser = require("body-parser");
var Router = express.Router();
var mysqlConnection = require("../connection");


Router.get("/result", (req,res)=> {

    mysqlConnection.query("SELECT *, (english + mathematics + physics + chemistry + biology) AS totalMarks, (english + mathematics + physics + chemistry + biology)/5 AS averageMarks, CASE WHEN (((english + mathematics + physics + chemistry + biology)/500)*100)>=80 THEN 'PASSED WITH DISTINCTION' WHEN (((english + mathematics + physics + chemistry + biology)/500)*100)>=60 THEN 'PASSED WITH FIRST CLASS' WHEN (((english + mathematics + physics + chemistry + biology)/500)*100)>=50 THEN 'PASSED WITH SECOND CLASS' WHEN (((english + mathematics + physics + chemistry + biology)/500)*100)>=40 THEN 'PASSED WITH THIRD CLASS' ELSE 'FAILED' END AS result FROM student", (err, rows, fields)=> {
        if(!err) {
         
            console.log(rows);
            res.send(rows);
                
        }
        else {
            console.log(err);
        }
    })
})

module.exports = Router;