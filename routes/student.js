var express = require("express");
var bodyParser = require("body-parser");
var Router = express.Router();
var mysqlConnection = require("../connection");

Router.post("/", (req, res)=> {
    console.log(req.body);
    var rollno = req.body.rollno;
    var firstname = req.body.firstname;
    var lastname = req.body.lastname;
    var eng = req.body.english;
    var maths = req.body.maths;
    var phy = req.body.physics;
    var che = req.body.chemistry;
    var bio = req.body.biology;

    if (typeof rollno=='number'&&typeof rollno!='string'&&rollno!=0&&rollno!=null) {

        mysqlConnection.query("SELECT * FROM student WHERE rollno = "+rollno+"", (err, rows, fields)=> {
            console.log(rows);
            if(rows&&rows.length) {

                console.log("Record with the given Rollno already exists!")    
                res.send("Record with the given Rollno already exists!");
    
            }
    
            else if(rollno&&firstname&&lastname&&eng&&maths&&phy&&che&&bio)
            {
                mysqlConnection.query("SELECT * FROM student WHERE rollno = "+rollno+"", (err, rows, fields)=> {
                    console.log(rows);

                    if (rows&&rows.length) {
                
                        res.send("Record with the given Rollno already exists!");
    
                    }

                    else {
    
                        mysqlConnection.query("INSERT INTO student (rollno, firstname, lastname, english, mathematics, physics, chemistry, biology) VALUES ("+rollno+", '"+firstname+"', '"+lastname+"',"+eng+", "+maths+", "+phy+", "+che+", "+bio+")", (err, rows, fields)=> {
                            console.log(err);
    
                            if(!err) {
    
                                // mysqlConnection.query("SELECT * FROM student", (err, rows, fields) => {

                                //     console.log(rows);

                                //     if(rows&&rows.length) {

                                //         res.send(rows);

                                //     }
                                //     else {

                                //         console.log(err);
                                //         res.send(err);
                                //     }

                                mysqlConnection.query("SELECT * FROM student WHERE rollno="+rollno+"", (err, rows, fields) => {

                                    console.log(rows);

                                    if(rows&&rows.length) {

                                        res.send(rows[0]);

                                    }
                                    else {

                                        console.log(err);
                                        res.send(err);
                                    }
                                });
    
                            }
    
                            else {
    
                                res.send(err);
    
                            }
                        })
                    }
                })
            }
            else {
                console.log("All fields are mandatory!");
                res.send("All fields are mandatory!");
            }
                
        })
    }

    else if(typeof rollno=='string') {

        console.log("Rollno must be a number!");
        res.send("Rollno must be a number!");

    }

    else if(rollno==0) {
        
        console.log("Rollno can't be 0!");
        res.send("Rollno can't be 0!");
    }
    
    else if(rollno==null) {

        console.log("Rollno can't be blank");
        res.send("Rollno can't be blank");

    }

    else {

        console.log("All fields are mandatory!");
        res.send("All fields are mandatory!");

    }
})


Router.patch("/:rollno", (req, res)=> {

    var rollno = parseInt(req.params.rollno);
    var firstname = req.body.firstname;
    var lastname = req.body.lastname;
    var eng = req.body.english;
    var maths = req.body.maths;
    var phy = req.body.physics;
    var che = req.body.chemistry;
    var bio = req.body.biology;

    if (rollno&&rollno!=0) {

        mysqlConnection.query("SELECT * FROM student WHERE rollno = "+rollno+"", (err, rows, fields)=> {
            console.log(rows);
            if(rollno&&rows&&rows.length) {

                if(firstname&&lastname&&eng&&maths&&phy&&che&&bio) {

                    mysqlConnection.query("UPDATE student SET firstname = '"+firstname+"', lastname = '"+lastname+"', english = "+eng+", mathematics = "+maths+", physics = "+phy+", chemistry = "+che+", biology = "+bio+" WHERE rollno = "+rollno+"", (err, rows, fields)=> {
                        console.log(err);
        
                        if(!err) {
        
                            mysqlConnection.query("SELECT * FROM student WHERE rollno="+rollno+"", (err, rows, fields) => {

                                console.log(rows);

                                if(rows&&rows.length) {

                                    res.send(rows[0]);

                                }
                                else {

                                    console.log(err);
                                    res.send(err);
                                }
                            })
        
                        }
        
                        else {
        
                            res.send(err);
        
                        }
                    })
                }

                else if(firstname&&lastname&&eng&&maths&&phy&&che) {

                    mysqlConnection.query("UPDATE student SET firstname = '"+firstname+"', lastname = '"+lastname+"', english = "+eng+", mathematics = "+maths+", physics = "+phy+", chemistry = "+che+" WHERE rollno = "+rollno+"", (err, rows, fields)=> {
                        console.log(err);
        
                        if(!err) {
        
                            mysqlConnection.query("SELECT * FROM student WHERE rollno="+rollno+"", (err, rows, fields) => {

                                console.log(rows);

                                if(rows&&rows.length) {

                                    res.send(rows[0]);

                                }
                                else {

                                    console.log(err);
                                    res.send(err);
                                }
                            })
        
                        }
        
                        else {
        
                            res.send(err);
        
                        }
                    })
                }

                else if(firstname&&lastname&&eng&&maths&&phy&&bio) {

                    mysqlConnection.query("UPDATE student SET firstname = '"+firstname+"', lastname = '"+lastname+"', english = "+eng+", mathematics = "+maths+", physics = "+phy+", biology = "+bio+" WHERE rollno = "+rollno+"", (err, rows, fields)=> {
                        console.log(err);
        
                        if(!err) {
        
                            mysqlConnection.query("SELECT * FROM student WHERE rollno="+rollno+"", (err, rows, fields) => {

                                console.log(rows);

                                if(rows&&rows.length) {

                                    res.send(rows[0]);

                                }
                                else {

                                    console.log(err);
                                    res.send(err);
                                }
                            })
        
                        }
        
                        else {
        
                            res.send(err);
        
                        }
                    })
                }

                else if(firstname&&lastname&&eng&&maths&&che&&bio) {

                    mysqlConnection.query("UPDATE student SET firstname = '"+firstname+"', lastname = '"+lastname+"', english = "+eng+", mathematics = "+maths+", chemistry = "+che+", biology = "+bio+" WHERE rollno = "+rollno+"", (err, rows, fields)=> {
                        console.log(err);
        
                        if(!err) {
        
                            mysqlConnection.query("SELECT * FROM student WHERE rollno="+rollno+"", (err, rows, fields) => {

                                console.log(rows);

                                if(rows&&rows.length) {

                                    res.send(rows[0]);

                                }
                                else {

                                    console.log(err);
                                    res.send(err);
                                }
                            })
        
                        }
        
                        else {
        
                            res.send(err);
        
                        }
                    })
                }

                else if(firstname&&lastname&&eng&&phy&&che&&bio) {

                    mysqlConnection.query("UPDATE student SET firstname = '"+firstname+"', lastname = '"+lastname+"', english = "+eng+", physics = "+phy+", chemistry = "+che+", biology = "+bio+" WHERE rollno = "+rollno+"", (err, rows, fields)=> {
                        console.log(err);
        
                        if(!err) {
        
                            mysqlConnection.query("SELECT * FROM student WHERE rollno="+rollno+"", (err, rows, fields) => {

                                console.log(rows);

                                if(rows&&rows.length) {

                                    res.send(rows[0]);

                                }
                                else {

                                    console.log(err);
                                    res.send(err);
                                }
                            })
        
                        }
        
                        else {
        
                            res.send(err);
        
                        }
                    })
                }

                else if(firstname&&lastname&&maths&&phy&&che&&bio) {

                    mysqlConnection.query("UPDATE student SET firstname = '"+firstname+"', lastname = '"+lastname+"', mathematics = "+maths+", physics = "+phy+", chemistry = "+che+", biology = "+bio+" WHERE rollno = "+rollno+"", (err, rows, fields)=> {
                        console.log(err);
        
                        if(!err) {
        
                            mysqlConnection.query("SELECT * FROM student WHERE rollno="+rollno+"", (err, rows, fields) => {

                                console.log(rows);

                                if(rows&&rows.length) {

                                    res.send(rows[0]);

                                }
                                else {

                                    console.log(err);
                                    res.send(err);
                                }
                            })
        
                        }
        
                        else {
        
                            res.send(err);
        
                        }
                    })
                }

                else if(firstname&&eng&&maths&&phy&&che&&bio) {

                    mysqlConnection.query("UPDATE student SET firstname = '"+firstname+"', english = "+eng+", mathematics = "+maths+", physics = "+phy+", chemistry = "+che+", biology = "+bio+" WHERE rollno = "+rollno+"", (err, rows, fields)=> {
                        console.log(err);
        
                        if(!err) {
        
                            mysqlConnection.query("SELECT * FROM student WHERE rollno="+rollno+"", (err, rows, fields) => {

                                console.log(rows);

                                if(rows&&rows.length) {

                                    res.send(rows[0]);

                                }
                                else {

                                    console.log(err);
                                    res.send(err);
                                }
                            })
        
                        }
        
                        else {
        
                            res.send(err);
        
                        }
                    })
                }

                else if(lastname&&eng&&maths&&phy&&che&&bio) {

                    mysqlConnection.query("UPDATE student SET lastname = '"+lastname+"', english = "+eng+", mathematics = "+maths+", physics = "+phy+", chemistry = "+che+", biology = "+bio+" WHERE rollno = "+rollno+"", (err, rows, fields)=> {
                        console.log(err);
        
                        if(!err) {
        
                            mysqlConnection.query("SELECT * FROM student WHERE rollno="+rollno+"", (err, rows, fields) => {

                                console.log(rows);

                                if(rows&&rows.length) {

                                    res.send(rows[0]);

                                }
                                else {

                                    console.log(err);
                                    res.send(err);
                                }
                            })
        
                        }
        
                        else {
        
                            res.send(err);
        
                        }
                    })
                }

                else if(firstname&&lastname&&eng&&maths&&phy) {

                    mysqlConnection.query("UPDATE student SET firstname = '"+firstname+"', lastname = '"+lastname+"', english = "+eng+", mathematics = "+maths+", physics = "+phy+" WHERE rollno = "+rollno+"", (err, rows, fields)=> {
                        console.log(err);
        
                        if(!err) {
        
                            mysqlConnection.query("SELECT * FROM student WHERE rollno="+rollno+"", (err, rows, fields) => {

                                console.log(rows);

                                if(rows&&rows.length) {

                                    res.send(rows[0]);

                                }
                                else {

                                    console.log(err);
                                    res.send(err);
                                }
                            })
        
                        }
        
                        else {
        
                            res.send(err);
        
                        }
                    })
                }

                else if(firstname&&lastname&&eng&&maths&&che) {

                    mysqlConnection.query("UPDATE student SET firstname = '"+firstname+"', lastname = '"+lastname+"', english = "+eng+", mathematics = "+maths+", physics = "+phy+", chemistry = "+che+", biology = "+bio+" WHERE rollno = "+rollno+"", (err, rows, fields)=> {
                        console.log(err);
        
                        if(!err) {
        
                            mysqlConnection.query("SELECT * FROM student WHERE rollno="+rollno+"", (err, rows, fields) => {

                                console.log(rows);

                                if(rows&&rows.length) {

                                    res.send(rows[0]);

                                }
                                else {

                                    console.log(err);
                                    res.send(err);
                                }
                            })
        
                        }
        
                        else {
        
                            res.send(err);
        
                        }
                    })
                }

                else if(firstname&&lastname&&eng&&phy&&che) {

                    mysqlConnection.query("UPDATE student SET firstname = '"+firstname+"', lastname = '"+lastname+"', english = "+eng+", physics = "+phy+", chemistry = "+che+" WHERE rollno = "+rollno+"", (err, rows, fields)=> {
                        console.log(err);
        
                        if(!err) {
        
                            mysqlConnection.query("SELECT * FROM student WHERE rollno="+rollno+"", (err, rows, fields) => {

                                console.log(rows);

                                if(rows&&rows.length) {

                                    res.send(rows[0]);

                                }
                                else {

                                    console.log(err);
                                    res.send(err);
                                }
                            })
        
                        }
        
                        else {
        
                            res.send(err);
        
                        }
                    })
                }

                else if(firstname&&lastname&&maths&&phy&&che) {

                    mysqlConnection.query("UPDATE student SET firstname = '"+firstname+"', lastname = '"+lastname+"', mathematics = "+maths+", physics = "+phy+", chemistry = "+che+" WHERE rollno = "+rollno+"", (err, rows, fields)=> {
                        console.log(err);
        
                        if(!err) {
        
                            mysqlConnection.query("SELECT * FROM student WHERE rollno="+rollno+"", (err, rows, fields) => {

                                console.log(rows);

                                if(rows&&rows.length) {

                                    res.send(rows[0]);

                                }
                                else {

                                    console.log(err);
                                    res.send(err);
                                }
                            })
        
                        }
        
                        else {
        
                            res.send(err);
        
                        }
                    })
                }

                else if(firstname&&eng&&maths&&phy&&che) {

                    mysqlConnection.query("UPDATE student SET firstname = '"+firstname+"', english = "+eng+", mathematics = "+maths+", physics = "+phy+", chemistry = "+che+" WHERE rollno = "+rollno+"", (err, rows, fields)=> {
                        console.log(err);
        
                        if(!err) {
        
                            mysqlConnection.query("SELECT * FROM student WHERE rollno="+rollno+"", (err, rows, fields) => {

                                console.log(rows);

                                if(rows&&rows.length) {

                                    res.send(rows[0]);

                                }
                                else {

                                    console.log(err);
                                    res.send(err);
                                }
                            })
        
                        }
        
                        else {
        
                            res.send(err);
        
                        }
                    })
                }

                else if(lastname&&eng&&maths&&phy&&che) {

                    mysqlConnection.query("UPDATE student SET lastname = '"+lastname+"', english = "+eng+", mathematics = "+maths+", physics = "+phy+", chemistry = "+che+" WHERE rollno = "+rollno+"", (err, rows, fields)=> {
                        console.log(err);
        
                        if(!err) {
        
                            mysqlConnection.query("SELECT * FROM student WHERE rollno="+rollno+"", (err, rows, fields) => {

                                console.log(rows);

                                if(rows&&rows.length) {

                                    res.send(rows[0]);

                                }
                                else {

                                    console.log(err);
                                    res.send(err);
                                }
                            })
        
                        }
        
                        else {
        
                            res.send(err);
        
                        }
                    })
                }

                else if(firstname&&lastname&&eng&&maths&&bio) {

                    mysqlConnection.query("UPDATE student SET firstname = '"+firstname+"', lastname = '"+lastname+"', english = "+eng+", mathematics = "+maths+", biology = "+bio+" WHERE rollno = "+rollno+"", (err, rows, fields)=> {
                        console.log(err);
        
                        if(!err) {
        
                            mysqlConnection.query("SELECT * FROM student WHERE rollno="+rollno+"", (err, rows, fields) => {

                                console.log(rows);

                                if(rows&&rows.length) {

                                    res.send(rows[0]);

                                }
                                else {

                                    console.log(err);
                                    res.send(err);
                                }
                            })
        
                        }
        
                        else {
        
                            res.send(err);
        
                        }
                    })
                }

                else if(firstname&&lastname&&eng&&phy&&bio) {

                    mysqlConnection.query("UPDATE student SET firstname = '"+firstname+"', lastname = '"+lastname+"', english = "+eng+", physics = "+phy+", biology = "+bio+" WHERE rollno = "+rollno+"", (err, rows, fields)=> {
                        console.log(err);
        
                        if(!err) {
        
                            mysqlConnection.query("SELECT * FROM student WHERE rollno="+rollno+"", (err, rows, fields) => {

                                console.log(rows);

                                if(rows&&rows.length) {

                                    res.send(rows[0]);

                                }
                                else {

                                    console.log(err);
                                    res.send(err);
                                }
                            })
        
                        }
        
                        else {
        
                            res.send(err);
        
                        }
                    })
                }

                else if(firstname&&lastname&&maths&&phy&&bio) {

                    mysqlConnection.query("UPDATE student SET firstname = '"+firstname+"', lastname = '"+lastname+"', mathematics = "+maths+", physics = "+phy+", biology = "+bio+" WHERE rollno = "+rollno+"", (err, rows, fields)=> {
                        console.log(err);
        
                        if(!err) {
        
                            mysqlConnection.query("SELECT * FROM student WHERE rollno="+rollno+"", (err, rows, fields) => {

                                console.log(rows);

                                if(rows&&rows.length) {

                                    res.send(rows[0]);

                                }
                                else {

                                    console.log(err);
                                    res.send(err);
                                }
                            })
        
                        }
        
                        else {
        
                            res.send(err);
        
                        }
                    })
                }

                else if(firstname&&eng&&maths&&phy&&bio) {

                    mysqlConnection.query("UPDATE student SET firstname = '"+firstname+"', english = "+eng+", mathematics = "+maths+", physics = "+phy+", biology = "+bio+" WHERE rollno = "+rollno+"", (err, rows, fields)=> {
                        console.log(err);
        
                        if(!err) {
        
                            mysqlConnection.query("SELECT * FROM student WHERE rollno="+rollno+"", (err, rows, fields) => {

                                console.log(rows);

                                if(rows&&rows.length) {

                                    res.send(rows[0]);

                                }
                                else {

                                    console.log(err);
                                    res.send(err);
                                }
                            })
        
                        }
        
                        else {
        
                            res.send(err);
        
                        }
                    })
                }

                else if(lastname&&eng&&maths&&phy&&bio) {

                    mysqlConnection.query("UPDATE student SET lastname = '"+lastname+"', english = "+eng+", mathematics = "+maths+", physics = "+phy+", biology = "+bio+" WHERE rollno = "+rollno+"", (err, rows, fields)=> {
                        console.log(err);
        
                        if(!err) {
        
                            mysqlConnection.query("SELECT * FROM student WHERE rollno="+rollno+"", (err, rows, fields) => {

                                console.log(rows);

                                if(rows&&rows.length) {

                                    res.send(rows[0]);

                                }
                                else {

                                    console.log(err);
                                    res.send(err);
                                }
                            })
        
                        }
        
                        else {
        
                            res.send(err);
        
                        }
                    })
                }

                else if(firstname&&lastname&&eng&&che&&bio) {

                    mysqlConnection.query("UPDATE student SET firstname = '"+firstname+"', lastname = '"+lastname+"', english = "+eng+", chemistry = "+che+", biology = "+bio+" WHERE rollno = "+rollno+"", (err, rows, fields)=> {
                        console.log(err);
        
                        if(!err) {
        
                            mysqlConnection.query("SELECT * FROM student WHERE rollno="+rollno+"", (err, rows, fields) => {

                                console.log(rows);

                                if(rows&&rows.length) {

                                    res.send(rows[0]);

                                }
                                else {

                                    console.log(err);
                                    res.send(err);
                                }
                            })
        
                        }
        
                        else {
        
                            res.send(err);
        
                        }
                    })
                }

                else if(firstname&&lastname&&maths&&che&&bio) {

                    mysqlConnection.query("UPDATE student SET firstname = '"+firstname+"', lastname = '"+lastname+"', mathematics = "+maths+", chemistry = "+che+", biology = "+bio+" WHERE rollno = "+rollno+"", (err, rows, fields)=> {
                        console.log(err);
        
                        if(!err) {
        
                            mysqlConnection.query("SELECT * FROM student WHERE rollno="+rollno+"", (err, rows, fields) => {

                                console.log(rows);

                                if(rows&&rows.length) {

                                    res.send(rows[0]);

                                }
                                else {

                                    console.log(err);
                                    res.send(err);
                                }
                            })
        
                        }
        
                        else {
        
                            res.send(err);
        
                        }
                    })
                }

                else if(firstname&&eng&&maths&&che&&bio) {

                    mysqlConnection.query("UPDATE student SET firstname = '"+firstname+"', english = "+eng+", mathematics = "+maths+", chemistry = "+che+", biology = "+bio+" WHERE rollno = "+rollno+"", (err, rows, fields)=> {
                        console.log(err);
        
                        if(!err) {
        
                            mysqlConnection.query("SELECT * FROM student WHERE rollno="+rollno+"", (err, rows, fields) => {

                                console.log(rows);

                                if(rows&&rows.length) {

                                    res.send(rows[0]);

                                }
                                else {

                                    console.log(err);
                                    res.send(err);
                                }
                            })
        
                        }
        
                        else {
        
                            res.send(err);
        
                        }
                    })
                }

                else if(lastname&&eng&&maths&&che&&bio) {

                    mysqlConnection.query("UPDATE student SET lastname = '"+lastname+"', english = "+eng+", mathematics = "+maths+", chemistry = "+che+", biology = "+bio+" WHERE rollno = "+rollno+"", (err, rows, fields)=> {
                        console.log(err);
        
                        if(!err) {
        
                            mysqlConnection.query("SELECT * FROM student WHERE rollno="+rollno+"", (err, rows, fields) => {

                                console.log(rows);

                                if(rows&&rows.length) {

                                    res.send(rows[0]);

                                }
                                else {

                                    console.log(err);
                                    res.send(err);
                                }
                            })
        
                        }
        
                        else {
        
                            res.send(err);
        
                        }
                    })
                }

                else if(firstname&&lastname&&phy&&che&&bio) {

                    mysqlConnection.query("UPDATE student SET firstname = '"+firstname+"', lastname = '"+lastname+"', physics = "+phy+", chemistry = "+che+", biology = "+bio+" WHERE rollno = "+rollno+"", (err, rows, fields)=> {
                        console.log(err);
        
                        if(!err) {
        
                            mysqlConnection.query("SELECT * FROM student WHERE rollno="+rollno+"", (err, rows, fields) => {

                                console.log(rows);

                                if(rows&&rows.length) {

                                    res.send(rows[0]);

                                }
                                else {

                                    console.log(err);
                                    res.send(err);
                                }
                            })
        
                        }
        
                        else {
        
                            res.send(err);
        
                        }
                    })
                }

                else if(firstname&&eng&&phy&&che&&bio) {

                    mysqlConnection.query("UPDATE student SET firstname = '"+firstname+"', english = "+eng+", physics = "+phy+", chemistry = "+che+", biology = "+bio+" WHERE rollno = "+rollno+"", (err, rows, fields)=> {
                        console.log(err);
        
                        if(!err) {
        
                            mysqlConnection.query("SELECT * FROM student WHERE rollno="+rollno+"", (err, rows, fields) => {

                                console.log(rows);

                                if(rows&&rows.length) {

                                    res.send(rows[0]);

                                }
                                else {

                                    console.log(err);
                                    res.send(err);
                                }
                            })
        
                        }
        
                        else {
        
                            res.send(err);
        
                        }
                    })
                }

                else if(lastname&&eng&&phy&&che&&bio) {

                    mysqlConnection.query("UPDATE student SET lastname = '"+lastname+"', english = "+eng+", physics = "+phy+", chemistry = "+che+", biology = "+bio+" WHERE rollno = "+rollno+"", (err, rows, fields)=> {
                        console.log(err);
        
                        if(!err) {
        
                            mysqlConnection.query("SELECT * FROM student WHERE rollno="+rollno+"", (err, rows, fields) => {

                                console.log(rows);

                                if(rows&&rows.length) {

                                    res.send(rows[0]);

                                }
                                else {

                                    console.log(err);
                                    res.send(err);
                                }
                            })
        
                        }
        
                        else {
        
                            res.send(err);
        
                        }
                    })
                }

                else if(firstname&&maths&&phy&&che&&bio) {

                    mysqlConnection.query("UPDATE student SET firstname = '"+firstname+"', mathematics = "+maths+", physics = "+phy+", chemistry = "+che+", biology = "+bio+" WHERE rollno = "+rollno+"", (err, rows, fields)=> {
                        console.log(err);
        
                        if(!err) {
        
                            mysqlConnection.query("SELECT * FROM student WHERE rollno="+rollno+"", (err, rows, fields) => {

                                console.log(rows);

                                if(rows&&rows.length) {

                                    res.send(rows[0]);

                                }
                                else {

                                    console.log(err);
                                    res.send(err);
                                }
                            })
        
                        }
        
                        else {
        
                            res.send(err);
        
                        }
                    })
                }

                else if(lastname&&maths&&phy&&che&&bio) {

                    mysqlConnection.query("UPDATE student SET lastname = '"+lastname+"', mathematics = "+maths+", physics = "+phy+", chemistry = "+che+", biology = "+bio+" WHERE rollno = "+rollno+"", (err, rows, fields)=> {
                        console.log(err);
        
                        if(!err) {
        
                            mysqlConnection.query("SELECT * FROM student WHERE rollno="+rollno+"", (err, rows, fields) => {

                                console.log(rows);

                                if(rows&&rows.length) {

                                    res.send(rows[0]);

                                }
                                else {

                                    console.log(err);
                                    res.send(err);
                                }
                            })
        
                        }
        
                        else {
        
                            res.send(err);
        
                        }
                    })
                }

                else if(eng&&maths&&phy&&che&&bio) {

                    mysqlConnection.query("UPDATE student SET english = "+eng+", mathematics = "+maths+", physics = "+phy+", chemistry = "+che+", biology = "+bio+" WHERE rollno = "+rollno+"", (err, rows, fields)=> {
                        console.log(err);
        
                        if(!err) {
        
                            mysqlConnection.query("SELECT * FROM student WHERE rollno="+rollno+"", (err, rows, fields) => {

                                console.log(rows);

                                if(rows&&rows.length) {

                                    res.send(rows[0]);

                                }
                                else {

                                    console.log(err);
                                    res.send(err);
                                }
                            })
        
                        }
        
                        else {
        
                            res.send(err);
        
                        }
                    })
                }

                else if(firstname&&lastname&&eng&&maths) {

                    mysqlConnection.query("UPDATE student SET firstname = '"+firstname+"', lastname = '"+lastname+"', english = "+eng+", mathematics = "+maths+" WHERE rollno = "+rollno+"", (err, rows, fields)=> {
                        console.log(err);
        
                        if(!err) {
        
                            mysqlConnection.query("SELECT * FROM student WHERE rollno="+rollno+"", (err, rows, fields) => {

                                console.log(rows);

                                if(rows&&rows.length) {

                                    res.send(rows[0]);

                                }
                                else {

                                    console.log(err);
                                    res.send(err);
                                }
                            })
        
                        }
        
                        else {
        
                            res.send(err);
        
                        }
                    })
                }

                else if(firstname&&lastname&&eng&&phy) {

                    mysqlConnection.query("UPDATE student SET firstname = '"+firstname+"', lastname = '"+lastname+"', english = "+eng+", physics = "+phy+" WHERE rollno = "+rollno+"", (err, rows, fields)=> {
                        console.log(err);
        
                        if(!err) {
        
                            mysqlConnection.query("SELECT * FROM student WHERE rollno="+rollno+"", (err, rows, fields) => {

                                console.log(rows);

                                if(rows&&rows.length) {

                                    res.send(rows[0]);

                                }
                                else {

                                    console.log(err);
                                    res.send(err);
                                }
                            })
        
                        }
        
                        else {
        
                            res.send(err);
        
                        }
                    })
                }

                else if(firstname&&lastname&&maths&&phy) {

                    mysqlConnection.query("UPDATE student SET firstname = '"+firstname+"', lastname = '"+lastname+"', mathematics = "+maths+", physics = "+phy+" WHERE rollno = "+rollno+"", (err, rows, fields)=> {
                        console.log(err);
        
                        if(!err) {
        
                            mysqlConnection.query("SELECT * FROM student WHERE rollno="+rollno+"", (err, rows, fields) => {

                                console.log(rows);

                                if(rows&&rows.length) {

                                    res.send(rows[0]);

                                }
                                else {

                                    console.log(err);
                                    res.send(err);
                                }
                            })
        
                        }
        
                        else {
        
                            res.send(err);
        
                        }
                    })
                }

                else if(firstname&&eng&&maths&&phy) {

                    mysqlConnection.query("UPDATE student SET firstname = '"+firstname+"', english = "+eng+", mathematics = "+maths+", physics = "+phy+" WHERE rollno = "+rollno+"", (err, rows, fields)=> {
                        console.log(err);
        
                        if(!err) {
        
                            mysqlConnection.query("SELECT * FROM student WHERE rollno="+rollno+"", (err, rows, fields) => {

                                console.log(rows);

                                if(rows&&rows.length) {

                                    res.send(rows[0]);

                                }
                                else {

                                    console.log(err);
                                    res.send(err);
                                }
                            })
        
                        }
        
                        else {
        
                            res.send(err);
        
                        }
                    })
                }

                else if(lastname&&eng&&maths&&phy) {

                    mysqlConnection.query("UPDATE student SET lastname = '"+lastname+"', english = "+eng+", mathematics = "+maths+", physics = "+phy+" WHERE rollno = "+rollno+"", (err, rows, fields)=> {
                        console.log(err);
        
                        if(!err) {
        
                            mysqlConnection.query("SELECT * FROM student WHERE rollno="+rollno+"", (err, rows, fields) => {

                                console.log(rows);

                                if(rows&&rows.length) {

                                    res.send(rows[0]);

                                }
                                else {

                                    console.log(err);
                                    res.send(err);
                                }
                            })
        
                        }
        
                        else {
        
                            res.send(err);
        
                        }
                    })
                }

                else if(firstname&&lastname&&eng&&che) {

                    mysqlConnection.query("UPDATE student SET firstname = '"+firstname+"', lastname = '"+lastname+"', english = "+eng+", chemistry = "+che+" WHERE rollno = "+rollno+"", (err, rows, fields)=> {
                        console.log(err);
        
                        if(!err) {
        
                            mysqlConnection.query("SELECT * FROM student WHERE rollno="+rollno+"", (err, rows, fields) => {

                                console.log(rows);

                                if(rows&&rows.length) {

                                    res.send(rows[0]);

                                }
                                else {

                                    console.log(err);
                                    res.send(err);
                                }
                            })
        
                        }
        
                        else {
        
                            res.send(err);
        
                        }
                    })
                }

                else if(firstname&&lastname&&maths&&che) {

                    mysqlConnection.query("UPDATE student SET firstname = '"+firstname+"', lastname = '"+lastname+"', mathematics = "+maths+", chemistry = "+che+" WHERE rollno = "+rollno+"", (err, rows, fields)=> {
                        console.log(err);
        
                        if(!err) {
        
                            mysqlConnection.query("SELECT * FROM student WHERE rollno="+rollno+"", (err, rows, fields) => {

                                console.log(rows);

                                if(rows&&rows.length) {

                                    res.send(rows[0]);

                                }
                                else {

                                    console.log(err);
                                    res.send(err);
                                }
                            })
        
                        }
        
                        else {
        
                            res.send(err);
        
                        }
                    })
                }

                else if(firstname&&eng&&maths&&che) {

                    mysqlConnection.query("UPDATE student SET firstname = '"+firstname+"', english = "+eng+", mathematics = "+maths+", chemistry = "+che+" WHERE rollno = "+rollno+"", (err, rows, fields)=> {
                        console.log(err);
        
                        if(!err) {
        
                            mysqlConnection.query("SELECT * FROM student WHERE rollno="+rollno+"", (err, rows, fields) => {

                                console.log(rows);

                                if(rows&&rows.length) {

                                    res.send(rows[0]);

                                }
                                else {

                                    console.log(err);
                                    res.send(err);
                                }
                            })
        
                        }
        
                        else {
        
                            res.send(err);
        
                        }
                    })
                }

                else if(lastname&&eng&&maths&&che) {

                    mysqlConnection.query("UPDATE student SET lastname = '"+lastname+"', english = "+eng+", mathematics = "+maths+", chemistry = "+che+" WHERE rollno = "+rollno+"", (err, rows, fields)=> {
                        console.log(err);
        
                        if(!err) {
        
                            mysqlConnection.query("SELECT * FROM student WHERE rollno="+rollno+"", (err, rows, fields) => {

                                console.log(rows);

                                if(rows&&rows.length) {

                                    res.send(rows[0]);

                                }
                                else {

                                    console.log(err);
                                    res.send(err);
                                }
                            })
        
                        }
        
                        else {
        
                            res.send(err);
        
                        }
                    })
                }

                else if(firstname&&lastname&&phy&&che) {

                    mysqlConnection.query("UPDATE student SET firstname = '"+firstname+"', lastname = '"+lastname+"', physics = "+phy+", chemistry = "+che+" WHERE rollno = "+rollno+"", (err, rows, fields)=> {
                        console.log(err);
        
                        if(!err) {
        
                            mysqlConnection.query("SELECT * FROM student WHERE rollno="+rollno+"", (err, rows, fields) => {

                                console.log(rows);

                                if(rows&&rows.length) {

                                    res.send(rows[0]);

                                }
                                else {

                                    console.log(err);
                                    res.send(err);
                                }
                            })
        
                        }
        
                        else {
        
                            res.send(err);
        
                        }
                    })
                }

                else if(firstname&&eng&&phy&&che) {

                    mysqlConnection.query("UPDATE student SET firstname = '"+firstname+"', english = "+eng+", physics = "+phy+", chemistry = "+che+" WHERE rollno = "+rollno+"", (err, rows, fields)=> {
                        console.log(err);
        
                        if(!err) {
        
                            mysqlConnection.query("SELECT * FROM student WHERE rollno="+rollno+"", (err, rows, fields) => {

                                console.log(rows);

                                if(rows&&rows.length) {

                                    res.send(rows[0]);

                                }
                                else {

                                    console.log(err);
                                    res.send(err);
                                }
                            })
        
                        }
        
                        else {
        
                            res.send(err);
        
                        }
                    })
                }

                else if(lastname&&eng&&phy&&che) {

                    mysqlConnection.query("UPDATE student SET lastname = '"+lastname+"', english = "+eng+", physics = "+phy+", chemistry = "+che+" WHERE rollno = "+rollno+"", (err, rows, fields)=> {
                        console.log(err);
        
                        if(!err) {
        
                            mysqlConnection.query("SELECT * FROM student WHERE rollno="+rollno+"", (err, rows, fields) => {

                                console.log(rows);

                                if(rows&&rows.length) {

                                    res.send(rows[0]);

                                }
                                else {

                                    console.log(err);
                                    res.send(err);
                                }
                            })
        
                        }
        
                        else {
        
                            res.send(err);
        
                        }
                    })
                }

                else if(firstname&&maths&&phy&&che) {

                    mysqlConnection.query("UPDATE student SET firstname = '"+firstname+"', mathematics = "+maths+", physics = "+phy+", chemistry = "+che+" WHERE rollno = "+rollno+"", (err, rows, fields)=> {
                        console.log(err);
        
                        if(!err) {
        
                            mysqlConnection.query("SELECT * FROM student WHERE rollno="+rollno+"", (err, rows, fields) => {

                                console.log(rows);

                                if(rows&&rows.length) {

                                    res.send(rows[0]);

                                }
                                else {

                                    console.log(err);
                                    res.send(err);
                                }
                            })
        
                        }
        
                        else {
        
                            res.send(err);
        
                        }
                    })
                }

                else if(lastname&&maths&&phy&&che) {

                    mysqlConnection.query("UPDATE student SET lastname = '"+lastname+"' mathematics = "+maths+", physics = "+phy+", chemistry = "+che+" WHERE rollno = "+rollno+"", (err, rows, fields)=> {
                        console.log(err);
        
                        if(!err) {
        
                            mysqlConnection.query("SELECT * FROM student WHERE rollno="+rollno+"", (err, rows, fields) => {

                                console.log(rows);

                                if(rows&&rows.length) {

                                    res.send(rows[0]);

                                }
                                else {

                                    console.log(err);
                                    res.send(err);
                                }
                            })
        
                        }
        
                        else {
        
                            res.send(err);
        
                        }
                    })
                }

                else if(eng&&maths&&phy&&che) {

                    mysqlConnection.query("UPDATE student SET english = "+eng+", mathematics = "+maths+", physics = "+phy+", chemistry = "+che+" WHERE rollno = "+rollno+"", (err, rows, fields)=> {
                        console.log(err);
        
                        if(!err) {
        
                            mysqlConnection.query("SELECT * FROM student WHERE rollno="+rollno+"", (err, rows, fields) => {

                                console.log(rows);

                                if(rows&&rows.length) {

                                    res.send(rows[0]);

                                }
                                else {

                                    console.log(err);
                                    res.send(err);
                                }
                            })
        
                        }
        
                        else {
        
                            res.send(err);
        
                        }
                    })
                }

                else if(firstname&&lastname&&eng&&bio) {

                    mysqlConnection.query("UPDATE student SET firstname = '"+firstname+"', lastname = '"+lastname+"', english = "+eng+", biology = "+bio+" WHERE rollno = "+rollno+"", (err, rows, fields)=> {
                        console.log(err);
        
                        if(!err) {
        
                            mysqlConnection.query("SELECT * FROM student WHERE rollno="+rollno+"", (err, rows, fields) => {

                                console.log(rows);

                                if(rows&&rows.length) {

                                    res.send(rows[0]);

                                }
                                else {

                                    console.log(err);
                                    res.send(err);
                                }
                            })
        
                        }
        
                        else {
        
                            res.send(err);
        
                        }
                    })
                }

                else if(firstname&&lastname&&maths&&bio) {

                    mysqlConnection.query("UPDATE student SET firstname = '"+firstname+"', lastname = '"+lastname+"', mathematics = "+maths+", biology = "+bio+" WHERE rollno = "+rollno+"", (err, rows, fields)=> {
                        console.log(err);
        
                        if(!err) {
        
                            mysqlConnection.query("SELECT * FROM student WHERE rollno="+rollno+"", (err, rows, fields) => {

                                console.log(rows);

                                if(rows&&rows.length) {

                                    res.send(rows[0]);

                                }
                                else {

                                    console.log(err);
                                    res.send(err);
                                }
                            })
        
                        }
        
                        else {
        
                            res.send(err);
        
                        }
                    })
                }

                else if(firstname&&eng&&maths&&bio) {

                    mysqlConnection.query("UPDATE student SET firstname = '"+firstname+"', english = "+eng+", mathematics = "+maths+", biology = "+bio+" WHERE rollno = "+rollno+"", (err, rows, fields)=> {
                        console.log(err);
        
                        if(!err) {
        
                            mysqlConnection.query("SELECT * FROM student WHERE rollno="+rollno+"", (err, rows, fields) => {

                                console.log(rows);

                                if(rows&&rows.length) {

                                    res.send(rows[0]);

                                }
                                else {

                                    console.log(err);
                                    res.send(err);
                                }
                            })
        
                        }
        
                        else {
        
                            res.send(err);
        
                        }
                    })
                }

                else if(lastname&&eng&&maths&&bio) {

                    mysqlConnection.query("UPDATE student SET lastname = '"+lastname+"', english = "+eng+", mathematics = "+maths+", biology = "+bio+" WHERE rollno = "+rollno+"", (err, rows, fields)=> {
                        console.log(err);
        
                        if(!err) {
        
                            mysqlConnection.query("SELECT * FROM student WHERE rollno="+rollno+"", (err, rows, fields) => {

                                console.log(rows);

                                if(rows&&rows.length) {

                                    res.send(rows[0]);

                                }
                                else {

                                    console.log(err);
                                    res.send(err);
                                }
                            })
        
                        }
        
                        else {
        
                            res.send(err);
        
                        }
                    })
                }

                else if(firstname&&lastname&&phy&&bio) {

                    mysqlConnection.query("UPDATE student SET firstname = '"+firstname+"', lastname = '"+lastname+"', physics = "+phy+", biology = "+bio+" WHERE rollno = "+rollno+"", (err, rows, fields)=> {
                        console.log(err);
        
                        if(!err) {
        
                            mysqlConnection.query("SELECT * FROM student WHERE rollno="+rollno+"", (err, rows, fields) => {

                                console.log(rows);

                                if(rows&&rows.length) {

                                    res.send(rows[0]);

                                }
                                else {

                                    console.log(err);
                                    res.send(err);
                                }
                            })
        
                        }
        
                        else {
        
                            res.send(err);
        
                        }
                    })
                }

                else if(firstname&&eng&&phy&&bio) {

                    mysqlConnection.query("UPDATE student SET firstname = '"+firstname+"', english = "+eng+", physics = "+phy+", biology = "+bio+" WHERE rollno = "+rollno+"", (err, rows, fields)=> {
                        console.log(err);
        
                        if(!err) {
        
                            mysqlConnection.query("SELECT * FROM student WHERE rollno="+rollno+"", (err, rows, fields) => {

                                console.log(rows);

                                if(rows&&rows.length) {

                                    res.send(rows[0]);

                                }
                                else {

                                    console.log(err);
                                    res.send(err);
                                }
                            })
        
                        }
        
                        else {
        
                            res.send(err);
        
                        }
                    })
                }

                else if(lastname&&eng&&phy&&bio) {

                    mysqlConnection.query("UPDATE student SET lastname = '"+lastname+"', english = "+eng+", physics = "+phy+", biology = "+bio+" WHERE rollno = "+rollno+"", (err, rows, fields)=> {
                        console.log(err);
        
                        if(!err) {
        
                            mysqlConnection.query("SELECT * FROM student WHERE rollno="+rollno+"", (err, rows, fields) => {

                                console.log(rows);

                                if(rows&&rows.length) {

                                    res.send(rows[0]);

                                }
                                else {

                                    console.log(err);
                                    res.send(err);
                                }
                            })
        
                        }
        
                        else {
        
                            res.send(err);
        
                        }
                    })
                }

                else if(firstname&&maths&&phy&&bio) {

                    mysqlConnection.query("UPDATE student SET firstname = '"+firstname+"', mathematics = "+maths+", physics = "+phy+", biology = "+bio+" WHERE rollno = "+rollno+"", (err, rows, fields)=> {
                        console.log(err);
        
                        if(!err) {
        
                            mysqlConnection.query("SELECT * FROM student WHERE rollno="+rollno+"", (err, rows, fields) => {

                                console.log(rows);

                                if(rows&&rows.length) {

                                    res.send(rows[0]);

                                }
                                else {

                                    console.log(err);
                                    res.send(err);
                                }
                            })
        
                        }
        
                        else {
        
                            res.send(err);
        
                        }
                    })
                }

                else if(lastname&&maths&&phy&&bio) {

                    mysqlConnection.query("UPDATE student SET lastname = '"+lastname+"', mathematics = "+maths+", physics = "+phy+", biology = "+bio+" WHERE rollno = "+rollno+"", (err, rows, fields)=> {
                        console.log(err);
        
                        if(!err) {
        
                            mysqlConnection.query("SELECT * FROM student WHERE rollno="+rollno+"", (err, rows, fields) => {

                                console.log(rows);

                                if(rows&&rows.length) {

                                    res.send(rows[0]);

                                }
                                else {

                                    console.log(err);
                                    res.send(err);
                                }
                            })
        
                        }
        
                        else {
        
                            res.send(err);
        
                        }
                    })
                }

                else if(eng&&maths&&phy&&bio) {

                    mysqlConnection.query("UPDATE student SET english = "+eng+", mathematics = "+maths+", physics = "+phy+", biology = "+bio+" WHERE rollno = "+rollno+"", (err, rows, fields)=> {
                        console.log(err);
        
                        if(!err) {
        
                            mysqlConnection.query("SELECT * FROM student WHERE rollno="+rollno+"", (err, rows, fields) => {

                                console.log(rows);

                                if(rows&&rows.length) {

                                    res.send(rows[0]);

                                }
                                else {

                                    console.log(err);
                                    res.send(err);
                                }
                            })
        
                        }
        
                        else {
        
                            res.send(err);
        
                        }
                    })
                }

                else if(firstname&&lastname&&che&&bio) {

                    mysqlConnection.query("UPDATE student SET firstname = '"+firstname+"', lastname = '"+lastname+"', chemistry = "+che+", biology = "+bio+" WHERE rollno = "+rollno+"", (err, rows, fields)=> {
                        console.log(err);
        
                        if(!err) {
        
                            mysqlConnection.query("SELECT * FROM student WHERE rollno="+rollno+"", (err, rows, fields) => {

                                console.log(rows);

                                if(rows&&rows.length) {

                                    res.send(rows[0]);

                                }
                                else {

                                    console.log(err);
                                    res.send(err);
                                }
                            })
        
                        }
        
                        else {
        
                            res.send(err);
        
                        }
                    })
                }

                else if(firstname&&eng&&che&&bio) {

                    mysqlConnection.query("UPDATE student SET firstname = '"+firstname+"', english = "+eng+", chemistry = "+che+", biology = "+bio+" WHERE rollno = "+rollno+"", (err, rows, fields)=> {
                        console.log(err);
        
                        if(!err) {
        
                            mysqlConnection.query("SELECT * FROM student WHERE rollno="+rollno+"", (err, rows, fields) => {

                                console.log(rows);

                                if(rows&&rows.length) {

                                    res.send(rows[0]);

                                }
                                else {

                                    console.log(err);
                                    res.send(err);
                                }
                            })
        
                        }
        
                        else {
        
                            res.send(err);
        
                        }
                    })
                }

                else if(lastname&&eng&&che&&bio) {

                    mysqlConnection.query("UPDATE student SET lastname = '"+lastname+"', english = "+eng+", chemistry = "+che+", biology = "+bio+" WHERE rollno = "+rollno+"", (err, rows, fields)=> {
                        console.log(err);
        
                        if(!err) {
        
                            mysqlConnection.query("SELECT * FROM student WHERE rollno="+rollno+"", (err, rows, fields) => {

                                console.log(rows);

                                if(rows&&rows.length) {

                                    res.send(rows[0]);

                                }
                                else {

                                    console.log(err);
                                    res.send(err);
                                }
                            })
        
                        }
        
                        else {
        
                            res.send(err);
        
                        }
                    })
                }

                else if(firstname&&maths&&che&&bio) {

                    mysqlConnection.query("UPDATE student SET firstname = '"+firstname+"', mathematics = "+maths+", chemistry = "+che+", biology = "+bio+" WHERE rollno = "+rollno+"", (err, rows, fields)=> {
                        console.log(err);
        
                        if(!err) {
        
                            mysqlConnection.query("SELECT * FROM student WHERE rollno="+rollno+"", (err, rows, fields) => {

                                console.log(rows);

                                if(rows&&rows.length) {

                                    res.send(rows[0]);

                                }
                                else {

                                    console.log(err);
                                    res.send(err);
                                }
                            })
        
                        }
        
                        else {
        
                            res.send(err);
        
                        }
                    })
                }

                else if(lastname&&maths&&che&&bio) {

                    mysqlConnection.query("UPDATE student SET lastname = '"+lastname+"', mathematics = "+maths+", chemistry = "+che+", biology = "+bio+" WHERE rollno = "+rollno+"", (err, rows, fields)=> {
                        console.log(err);
        
                        if(!err) {
        
                            mysqlConnection.query("SELECT * FROM student WHERE rollno="+rollno+"", (err, rows, fields) => {

                                console.log(rows);

                                if(rows&&rows.length) {

                                    res.send(rows[0]);

                                }
                                else {

                                    console.log(err);
                                    res.send(err);
                                }
                            })
        
                        }
        
                        else {
        
                            res.send(err);
        
                        }
                    })
                }

                else if(eng&&maths&&che&&bio) {

                    mysqlConnection.query("UPDATE student SET english = "+eng+", mathematics = "+maths+", chemistry = "+che+", biology = "+bio+" WHERE rollno = "+rollno+"", (err, rows, fields)=> {
                        console.log(err);
        
                        if(!err) {
        
                            mysqlConnection.query("SELECT * FROM student WHERE rollno="+rollno+"", (err, rows, fields) => {

                                console.log(rows);

                                if(rows&&rows.length) {

                                    res.send(rows[0]);

                                }
                                else {

                                    console.log(err);
                                    res.send(err);
                                }
                            })
        
                        }
        
                        else {
        
                            res.send(err);
        
                        }
                    })
                }

                else if(firstname&&phy&&che&&bio) {

                    mysqlConnection.query("UPDATE student SET firstname = '"+firstname+"', physics = "+phy+", chemistry = "+che+", biology = "+bio+" WHERE rollno = "+rollno+"", (err, rows, fields)=> {
                        console.log(err);
        
                        if(!err) {
        
                            mysqlConnection.query("SELECT * FROM student WHERE rollno="+rollno+"", (err, rows, fields) => {

                                console.log(rows);

                                if(rows&&rows.length) {

                                    res.send(rows[0]);

                                }
                                else {

                                    console.log(err);
                                    res.send(err);
                                }
                            })
        
                        }
        
                        else {
        
                            res.send(err);
        
                        }
                    })
                }

                else if(lastname&&phy&&che&&bio) {

                    mysqlConnection.query("UPDATE student SET lastname = '"+lastname+"', physics = "+phy+", chemistry = "+che+", biology = "+bio+" WHERE rollno = "+rollno+"", (err, rows, fields)=> {
                        console.log(err);
        
                        if(!err) {
        
                            mysqlConnection.query("SELECT * FROM student WHERE rollno="+rollno+"", (err, rows, fields) => {

                                console.log(rows);

                                if(rows&&rows.length) {

                                    res.send(rows[0]);

                                }
                                else {

                                    console.log(err);
                                    res.send(err);
                                }
                            })
        
                        }
        
                        else {
        
                            res.send(err);
        
                        }
                    })
                }

                else if(eng&&phy&&che&&bio) {

                    mysqlConnection.query("UPDATE student SET english = "+eng+", physics = "+phy+", chemistry = "+che+", biology = "+bio+" WHERE rollno = "+rollno+"", (err, rows, fields)=> {
                        console.log(err);
        
                        if(!err) {
        
                            mysqlConnection.query("SELECT * FROM student WHERE rollno="+rollno+"", (err, rows, fields) => {

                                console.log(rows);

                                if(rows&&rows.length) {

                                    res.send(rows[0]);

                                }
                                else {

                                    console.log(err);
                                    res.send(err);
                                }
                            })
        
                        }
        
                        else {
        
                            res.send(err);
        
                        }
                    })
                }

                else if(maths&&phy&&che&&bio) {

                    mysqlConnection.query("UPDATE student SET mathematics = "+maths+", physics = "+phy+", chemistry = "+che+", biology = "+bio+" WHERE rollno = "+rollno+"", (err, rows, fields)=> {
                        console.log(err);
        
                        if(!err) {
        
                            mysqlConnection.query("SELECT * FROM student WHERE rollno="+rollno+"", (err, rows, fields) => {

                                console.log(rows);

                                if(rows&&rows.length) {

                                    res.send(rows[0]);

                                }
                                else {

                                    console.log(err);
                                    res.send(err);
                                }
                            })
        
                        }
        
                        else {
        
                            res.send(err);
        
                        }
                    })
                }

                else if(firstname&&lastname&&eng) {

                    mysqlConnection.query("UPDATE student SET firstname = '"+firstname+"', lastname = '"+lastname+"', english = "+eng+" WHERE rollno = "+rollno+"", (err, rows, fields)=> {
                        console.log(err);
        
                        if(!err) {
        
                            mysqlConnection.query("SELECT * FROM student WHERE rollno="+rollno+"", (err, rows, fields) => {

                                console.log(rows);

                                if(rows&&rows.length) {

                                    res.send(rows[0]);

                                }
                                else {

                                    console.log(err);
                                    res.send(err);
                                }
                            })
        
                        }
        
                        else {
        
                            res.send(err);
        
                        }
                    })
                }

                else if(firstname&&lastname&&maths) {

                    mysqlConnection.query("UPDATE student SET firstname = '"+firstname+"', lastname = '"+lastname+"', mathematics = "+maths+" WHERE rollno = "+rollno+"", (err, rows, fields)=> {
                        console.log(err);
        
                        if(!err) {
        
                            mysqlConnection.query("SELECT * FROM student WHERE rollno="+rollno+"", (err, rows, fields) => {

                                console.log(rows);

                                if(rows&&rows.length) {

                                    res.send(rows[0]);

                                }
                                else {

                                    console.log(err);
                                    res.send(err);
                                }
                            })
        
                        }
        
                        else {
        
                            res.send(err);
        
                        }
                    })
                }

                else if(firstname&&eng&&maths) {

                    mysqlConnection.query("UPDATE student SET firstname = '"+firstname+"', english = "+eng+", mathematics = "+maths+" WHERE rollno = "+rollno+"", (err, rows, fields)=> {
                        console.log(err);
        
                        if(!err) {
        
                            mysqlConnection.query("SELECT * FROM student WHERE rollno="+rollno+"", (err, rows, fields) => {

                                console.log(rows);

                                if(rows&&rows.length) {

                                    res.send(rows[0]);

                                }
                                else {

                                    console.log(err);
                                    res.send(err);
                                }
                            })
        
                        }
        
                        else {
        
                            res.send(err);
        
                        }
                    })
                }

                else if(lastname&&eng&&maths) {

                    mysqlConnection.query("UPDATE student SET lastname = '"+lastname+"', english = "+eng+", mathematics = "+maths+" WHERE rollno = "+rollno+"", (err, rows, fields)=> {
                        console.log(err);
        
                        if(!err) {
        
                            mysqlConnection.query("SELECT * FROM student WHERE rollno="+rollno+"", (err, rows, fields) => {

                                console.log(rows);

                                if(rows&&rows.length) {

                                    res.send(rows[0]);

                                }
                                else {

                                    console.log(err);
                                    res.send(err);
                                }
                            })
        
                        }
        
                        else {
        
                            res.send(err);
        
                        }
                    })
                }

                else if(firstname&&lastname&&phy) {

                    mysqlConnection.query("UPDATE student SET firstname = '"+firstname+"', lastname = '"+lastname+"', physics = "+phy+" WHERE rollno = "+rollno+"", (err, rows, fields)=> {
                        console.log(err);
        
                        if(!err) {
        
                            mysqlConnection.query("SELECT * FROM student WHERE rollno="+rollno+"", (err, rows, fields) => {

                                console.log(rows);

                                if(rows&&rows.length) {

                                    res.send(rows[0]);

                                }
                                else {

                                    console.log(err);
                                    res.send(err);
                                }
                            })
        
                        }
        
                        else {
        
                            res.send(err);
        
                        }
                    })
                }

                else if(firstname&&eng&&phy) {

                    mysqlConnection.query("UPDATE student SET firstname = '"+firstname+"', english = "+eng+", physics = "+phy+" WHERE rollno = "+rollno+"", (err, rows, fields)=> {
                        console.log(err);
        
                        if(!err) {
        
                            mysqlConnection.query("SELECT * FROM student WHERE rollno="+rollno+"", (err, rows, fields) => {

                                console.log(rows);

                                if(rows&&rows.length) {

                                    res.send(rows[0]);

                                }
                                else {

                                    console.log(err);
                                    res.send(err);
                                }
                            })
        
                        }
        
                        else {
        
                            res.send(err);
        
                        }
                    })
                }

                else if(lastname&&eng&&phy) {

                    mysqlConnection.query("UPDATE student SET lastname = '"+lastname+"', english = "+eng+", physics = "+phy+" WHERE rollno = "+rollno+"", (err, rows, fields)=> {
                        console.log(err);
        
                        if(!err) {
        
                            mysqlConnection.query("SELECT * FROM student WHERE rollno="+rollno+"", (err, rows, fields) => {

                                console.log(rows);

                                if(rows&&rows.length) {

                                    res.send(rows[0]);

                                }
                                else {

                                    console.log(err);
                                    res.send(err);
                                }
                            })
        
                        }
        
                        else {
        
                            res.send(err);
        
                        }
                    })
                }

                else if(firstname&&maths&&phy) {

                    mysqlConnection.query("UPDATE student SET firstname = '"+firstname+"', mathematics = "+maths+", physics = "+phy+" WHERE rollno = "+rollno+"", (err, rows, fields)=> {
                        console.log(err);
        
                        if(!err) {
        
                            mysqlConnection.query("SELECT * FROM student WHERE rollno="+rollno+"", (err, rows, fields) => {

                                console.log(rows);

                                if(rows&&rows.length) {

                                    res.send(rows[0]);

                                }
                                else {

                                    console.log(err);
                                    res.send(err);
                                }
                            })
        
                        }
        
                        else {
        
                            res.send(err);
        
                        }
                    })
                }

                else if(lastname&&maths&&phy) {

                    mysqlConnection.query("UPDATE student SET lastname = '"+lastname+"', mathematics = "+maths+", physics = "+phy+" WHERE rollno = "+rollno+"", (err, rows, fields)=> {
                        console.log(err);
        
                        if(!err) {
        
                            mysqlConnection.query("SELECT * FROM student WHERE rollno="+rollno+"", (err, rows, fields) => {

                                console.log(rows);

                                if(rows&&rows.length) {

                                    res.send(rows[0]);

                                }
                                else {

                                    console.log(err);
                                    res.send(err);
                                }
                            })
        
                        }
        
                        else {
        
                            res.send(err);
        
                        }
                    })
                }

                else if(eng&&maths&&phy) {

                    mysqlConnection.query("UPDATE student SET english = "+eng+", mathematics = "+maths+", physics = "+phy+" WHERE rollno = "+rollno+"", (err, rows, fields)=> {
                        console.log(err);
        
                        if(!err) {
        
                            mysqlConnection.query("SELECT * FROM student WHERE rollno="+rollno+"", (err, rows, fields) => {

                                console.log(rows);

                                if(rows&&rows.length) {

                                    res.send(rows[0]);

                                }
                                else {

                                    console.log(err);
                                    res.send(err);
                                }
                            })
        
                        }
        
                        else {
        
                            res.send(err);
        
                        }
                    })
                }

                else if(firstname&&lastname&&che) {

                    mysqlConnection.query("UPDATE student SET firstname = '"+firstname+"', lastname = '"+lastname+"', chemistry = "+che+" WHERE rollno = "+rollno+"", (err, rows, fields)=> {
                        console.log(err);
        
                        if(!err) {
        
                            mysqlConnection.query("SELECT * FROM student WHERE rollno="+rollno+"", (err, rows, fields) => {

                                console.log(rows);

                                if(rows&&rows.length) {

                                    res.send(rows[0]);

                                }
                                else {

                                    console.log(err);
                                    res.send(err);
                                }
                            })
        
                        }
        
                        else {
        
                            res.send(err);
        
                        }
                    })
                }

                else if(firstname&&eng&&che) {

                    mysqlConnection.query("UPDATE student SET firstname = '"+firstname+"', english = "+eng+", chemistry = "+che+" WHERE rollno = "+rollno+"", (err, rows, fields)=> {
                        console.log(err);
        
                        if(!err) {
        
                            mysqlConnection.query("SELECT * FROM student WHERE rollno="+rollno+"", (err, rows, fields) => {

                                console.log(rows);

                                if(rows&&rows.length) {

                                    res.send(rows[0]);

                                }
                                else {

                                    console.log(err);
                                    res.send(err);
                                }
                            })
        
                        }
        
                        else {
        
                            res.send(err);
        
                        }
                    })
                }

                else if(lastname&&eng&&che) {

                    mysqlConnection.query("UPDATE student SET lastname = '"+lastname+"', english = "+eng+", chemistry = "+che+" WHERE rollno = "+rollno+"", (err, rows, fields)=> {
                        console.log(err);
        
                        if(!err) {
        
                            mysqlConnection.query("SELECT * FROM student WHERE rollno="+rollno+"", (err, rows, fields) => {

                                console.log(rows);

                                if(rows&&rows.length) {

                                    res.send(rows[0]);

                                }
                                else {

                                    console.log(err);
                                    res.send(err);
                                }
                            })
        
                        }
        
                        else {
        
                            res.send(err);
        
                        }
                    })
                }

                else if(firstname&&maths&&che) {

                    mysqlConnection.query("UPDATE student SET firstname = '"+firstname+"', mathematics = "+maths+", chemistry = "+che+" WHERE rollno = "+rollno+"", (err, rows, fields)=> {
                        console.log(err);
        
                        if(!err) {
        
                            mysqlConnection.query("SELECT * FROM student WHERE rollno="+rollno+"", (err, rows, fields) => {

                                console.log(rows);

                                if(rows&&rows.length) {

                                    res.send(rows[0]);

                                }
                                else {

                                    console.log(err);
                                    res.send(err);
                                }
                            })
                        }
        
                        else {
        
                            res.send(err);
        
                        }
                    })
                }

                else if(lastname&&maths&&che) {

                    mysqlConnection.query("UPDATE student SET lastname = '"+lastname+"', mathematics = "+maths+", chemistry = "+che+" WHERE rollno = "+rollno+"", (err, rows, fields)=> {
                        console.log(err);
        
                        if(!err) {
        
                            mysqlConnection.query("SELECT * FROM student WHERE rollno="+rollno+"", (err, rows, fields) => {

                                console.log(rows);

                                if(rows&&rows.length) {

                                    res.send(rows[0]);

                                }
                                else {

                                    console.log(err);
                                    res.send(err);
                                }
                            })
        
                        }
        
                        else {
        
                            res.send(err);
        
                        }
                    })
                }

                else if(eng&&maths&&che) {

                    mysqlConnection.query("UPDATE student SET english = "+eng+", mathematics = "+maths+", chemistry = "+che+" WHERE rollno = "+rollno+"", (err, rows, fields)=> {
                        console.log(err);
        
                        if(!err) {
        
                            mysqlConnection.query("SELECT * FROM student WHERE rollno="+rollno+"", (err, rows, fields) => {

                                console.log(rows);

                                if(rows&&rows.length) {

                                    res.send(rows[0]);

                                }
                                else {

                                    console.log(err);
                                    res.send(err);
                                }
                            })
        
                        }
        
                        else {
        
                            res.send(err);
        
                        }
                    })
                }

                else if(firstname&&phy&&che) {

                    mysqlConnection.query("UPDATE student SET firstname = '"+firstname+"', physics = "+phy+", chemistry = "+che+" WHERE rollno = "+rollno+"", (err, rows, fields)=> {
                        console.log(err);
        
                        if(!err) {
        
                            mysqlConnection.query("SELECT * FROM student WHERE rollno="+rollno+"", (err, rows, fields) => {

                                console.log(rows);

                                if(rows&&rows.length) {

                                    res.send(rows[0]);

                                }
                                else {

                                    console.log(err);
                                    res.send(err);
                                }
                            })
        
                        }
        
                        else {
        
                            res.send(err);
        
                        }
                    })
                }

                else if(lastname&&phy&&che) {

                    mysqlConnection.query("UPDATE student SET lastname = '"+lastname+"', physics = "+phy+", chemistry = "+che+" WHERE rollno = "+rollno+"", (err, rows, fields)=> {
                        console.log(err);
        
                        if(!err) {
        
                            mysqlConnection.query("SELECT * FROM student WHERE rollno="+rollno+"", (err, rows, fields) => {

                                console.log(rows);

                                if(rows&&rows.length) {

                                    res.send(rows[0]);

                                }
                                else {

                                    console.log(err);
                                    res.send(err);
                                }
                            })
        
                        }
        
                        else {
        
                            res.send(err);
        
                        }
                    })
                }

                else if(eng&&phy&&che) {

                    mysqlConnection.query("UPDATE student SET english = "+eng+", physics = "+phy+", chemistry = "+che+" WHERE rollno = "+rollno+"", (err, rows, fields)=> {
                        console.log(err);
        
                        if(!err) {
        
                            mysqlConnection.query("SELECT * FROM student WHERE rollno="+rollno+"", (err, rows, fields) => {

                                console.log(rows);

                                if(rows&&rows.length) {

                                    res.send(rows[0]);

                                }
                                else {

                                    console.log(err);
                                    res.send(err);
                                }
                            })
        
                        }
        
                        else {
        
                            res.send(err);
        
                        }
                    })
                }

                else if(maths&&phy&&che) {

                    mysqlConnection.query("UPDATE student SET mathematics = "+maths+", physics = "+phy+", chemistry = "+che+" WHERE rollno = "+rollno+"", (err, rows, fields)=> {
                        console.log(err);
        
                        if(!err) {
        
                            mysqlConnection.query("SELECT * FROM student WHERE rollno="+rollno+"", (err, rows, fields) => {

                                console.log(rows);

                                if(rows&&rows.length) {

                                    res.send(rows[0]);

                                }
                                else {

                                    console.log(err);
                                    res.send(err);
                                }
                            })
        
                        }
        
                        else {
        
                            res.send(err);
        
                        }
                    })
                }

                else if(firstname&&lastname&&bio) {

                    mysqlConnection.query("UPDATE student SET firstname = '"+firstname+"', lastname = '"+lastname+"', biology = "+bio+" WHERE rollno = "+rollno+"", (err, rows, fields)=> {
                        console.log(err);
        
                        if(!err) {
        
                            mysqlConnection.query("SELECT * FROM student WHERE rollno="+rollno+"", (err, rows, fields) => {

                                console.log(rows);

                                if(rows&&rows.length) {

                                    res.send(rows[0]);

                                }
                                else {

                                    console.log(err);
                                    res.send(err);
                                }
                            })
        
                        }
        
                        else {
        
                            res.send(err);
        
                        }
                    })
                }

                else if(firstname&&eng&&bio) {

                    mysqlConnection.query("UPDATE student SET firstname = '"+firstname+"', english = "+eng+", biology = "+bio+" WHERE rollno = "+rollno+"", (err, rows, fields)=> {
                        console.log(err);
        
                        if(!err) {
        
                            mysqlConnection.query("SELECT * FROM student WHERE rollno="+rollno+"", (err, rows, fields) => {

                                console.log(rows);

                                if(rows&&rows.length) {

                                    res.send(rows[0]);

                                }
                                else {

                                    console.log(err);
                                    res.send(err);
                                }
                            })
        
                        }
        
                        else {
        
                            res.send(err);
        
                        }
                    })
                }

                else if(lastname&&eng&&bio) {

                    mysqlConnection.query("UPDATE student SET lastname = '"+lastname+"', english = "+eng+", biology = "+bio+" WHERE rollno = "+rollno+"", (err, rows, fields)=> {
                        console.log(err);
        
                        if(!err) {
        
                            mysqlConnection.query("SELECT * FROM student WHERE rollno="+rollno+"", (err, rows, fields) => {

                                console.log(rows);

                                if(rows&&rows.length) {

                                    res.send(rows[0]);

                                }
                                else {

                                    console.log(err);
                                    res.send(err);
                                }
                            })
        
                        }
        
                        else {
        
                            res.send(err);
        
                        }
                    })
                }

                else if(firstname&&maths&&bio) {

                    mysqlConnection.query("UPDATE student SET firstname = '"+firstname+"', mathematics = "+maths+", biology = "+bio+" WHERE rollno = "+rollno+"", (err, rows, fields)=> {
                        console.log(err);
        
                        if(!err) {
        
                            mysqlConnection.query("SELECT * FROM student WHERE rollno="+rollno+"", (err, rows, fields) => {

                                console.log(rows);

                                if(rows&&rows.length) {

                                    res.send(rows[0]);

                                }
                                else {

                                    console.log(err);
                                    res.send(err);
                                }
                            })
        
                        }
        
                        else {
        
                            res.send(err);
        
                        }
                    })
                }

                else if(lastname&&maths&&bio) {

                    mysqlConnection.query("UPDATE student SET lastname = '"+lastname+"', mathematics = "+maths+", biology = "+bio+" WHERE rollno = "+rollno+"", (err, rows, fields)=> {
                        console.log(err);
        
                        if(!err) {
        
                            mysqlConnection.query("SELECT * FROM student WHERE rollno="+rollno+"", (err, rows, fields) => {

                                console.log(rows);

                                if(rows&&rows.length) {

                                    res.send(rows[0]);

                                }
                                else {

                                    console.log(err);
                                    res.send(err);
                                }
                            })
        
                        }
        
                        else {
        
                            res.send(err);
        
                        }
                    })
                }

                else if(eng&&maths&&bio) {

                    mysqlConnection.query("UPDATE student SET english = "+eng+", mathematics = "+maths+", biology = "+bio+" WHERE rollno = "+rollno+"", (err, rows, fields)=> {
                        console.log(err);
        
                        if(!err) {
        
                            mysqlConnection.query("SELECT * FROM student WHERE rollno="+rollno+"", (err, rows, fields) => {

                                console.log(rows);

                                if(rows&&rows.length) {

                                    res.send(rows[0]);

                                }
                                else {

                                    console.log(err);
                                    res.send(err);
                                }
                            })
        
                        }
        
                        else {
        
                            res.send(err);
        
                        }
                    })
                }

                else if(firstname&&phy&&bio) {

                    mysqlConnection.query("UPDATE student SET firstname = '"+firstname+"', physics = "+phy+", biology = "+bio+" WHERE rollno = "+rollno+"", (err, rows, fields)=> {
                        console.log(err);
        
                        if(!err) {
        
                            mysqlConnection.query("SELECT * FROM student WHERE rollno="+rollno+"", (err, rows, fields) => {

                                console.log(rows);

                                if(rows&&rows.length) {

                                    res.send(rows[0]);

                                }
                                else {

                                    console.log(err);
                                    res.send(err);
                                }
                            })
        
                        }
        
                        else {
        
                            res.send(err);
        
                        }
                    })
                }

                else if(lastname&&phy&&bio) {

                    mysqlConnection.query("UPDATE student SET lastname = '"+lastname+"', physics = "+phy+", biology = "+bio+" WHERE rollno = "+rollno+"", (err, rows, fields)=> {
                        console.log(err);
        
                        if(!err) {
        
                            mysqlConnection.query("SELECT * FROM student WHERE rollno="+rollno+"", (err, rows, fields) => {

                                console.log(rows);

                                if(rows&&rows.length) {

                                    res.send(rows[0]);

                                }
                                else {

                                    console.log(err);
                                    res.send(err);
                                }
                            })
        
                        }
        
                        else {
        
                            res.send(err);
        
                        }
                    })
                }

                else if(eng&&phy&&bio) {

                    mysqlConnection.query("UPDATE student SET english = "+eng+", physics = "+phy+", biology = "+bio+" WHERE rollno = "+rollno+"", (err, rows, fields)=> {
                        console.log(err);
        
                        if(!err) {
        
                            mysqlConnection.query("SELECT * FROM student WHERE rollno="+rollno+"", (err, rows, fields) => {

                                console.log(rows);

                                if(rows&&rows.length) {

                                    res.send(rows[0]);

                                }
                                else {

                                    console.log(err);
                                    res.send(err);
                                }
                            })
        
                        }
        
                        else {
        
                            res.send(err);
        
                        }
                    })
                }

                else if(maths&&phy&&bio) {

                    mysqlConnection.query("UPDATE student SET mathematics = "+maths+", physics = "+phy+", biology = "+bio+" WHERE rollno = "+rollno+"", (err, rows, fields)=> {
                        console.log(err);
        
                        if(!err) {
        
                            mysqlConnection.query("SELECT * FROM student WHERE rollno="+rollno+"", (err, rows, fields) => {

                                console.log(rows);

                                if(rows&&rows.length) {

                                    res.send(rows[0]);

                                }
                                else {

                                    console.log(err);
                                    res.send(err);
                                }
                            })
        
                        }
        
                        else {
        
                            res.send(err);
        
                        }
                    })
                }

                else if(firstname&&che&&bio) {

                    mysqlConnection.query("UPDATE student SET firstname = '"+firstname+"', chemistry = "+che+", biology = "+bio+" WHERE rollno = "+rollno+"", (err, rows, fields)=> {
                        console.log(err);
        
                        if(!err) {
        
                            mysqlConnection.query("SELECT * FROM student WHERE rollno="+rollno+"", (err, rows, fields) => {

                                console.log(rows);

                                if(rows&&rows.length) {

                                    res.send(rows[0]);

                                }
                                else {

                                    console.log(err);
                                    res.send(err);
                                }
                            })
        
                        }
        
                        else {
        
                            res.send(err);
        
                        }
                    })
                }

                else if(lastname&&che&&bio) {

                    mysqlConnection.query("UPDATE student SET lastname = '"+lastname+"', chemistry = "+che+", biology = "+bio+" WHERE rollno = "+rollno+"", (err, rows, fields)=> {
                        console.log(err);
        
                        if(!err) {
        
                            mysqlConnection.query("SELECT * FROM student WHERE rollno="+rollno+"", (err, rows, fields) => {

                                console.log(rows);

                                if(rows&&rows.length) {

                                    res.send(rows[0]);

                                }
                                else {

                                    console.log(err);
                                    res.send(err);
                                }
                            })
        
                        }
        
                        else {
        
                            res.send(err);
        
                        }
                    })
                }

                else if(eng&&che&&bio) {

                    mysqlConnection.query("UPDATE student SET english = "+eng+", chemistry = "+che+", biology = "+bio+" WHERE rollno = "+rollno+"", (err, rows, fields)=> {
                        console.log(err);
        
                        if(!err) {
        
                            mysqlConnection.query("SELECT * FROM student WHERE rollno="+rollno+"", (err, rows, fields) => {

                                console.log(rows);

                                if(rows&&rows.length) {

                                    res.send(rows[0]);

                                }
                                else {

                                    console.log(err);
                                    res.send(err);
                                }
                            })
        
                        }
        
                        else {
        
                            res.send(err);
        
                        }
                    })
                }

                else if(maths&&che&&bio) {

                    mysqlConnection.query("UPDATE student SET mathematics = "+maths+", chemistry = "+che+", biology = "+bio+" WHERE rollno = "+rollno+"", (err, rows, fields)=> {
                        console.log(err);
        
                        if(!err) {
        
                            mysqlConnection.query("SELECT * FROM student WHERE rollno="+rollno+"", (err, rows, fields) => {

                                console.log(rows);

                                if(rows&&rows.length) {

                                    res.send(rows[0]);

                                }
                                else {

                                    console.log(err);
                                    res.send(err);
                                }
                            })
        
                        }
        
                        else {
        
                            res.send(err);
        
                        }
                    })
                }

                else if(phy&&che&&bio) {

                    mysqlConnection.query("UPDATE student SET physics = "+phy+", chemistry = "+che+", biology = "+bio+" WHERE rollno = "+rollno+"", (err, rows, fields)=> {
                        console.log(err);
        
                        if(!err) {
        
                            mysqlConnection.query("SELECT * FROM student WHERE rollno="+rollno+"", (err, rows, fields) => {

                                console.log(rows);

                                if(rows&&rows.length) {

                                    res.send(rows[0]);

                                }
                                else {

                                    console.log(err);
                                    res.send(err);
                                }
                            })
        
                        }
        
                        else {
        
                            res.send(err);
        
                        }
                    })
                }

                else if(firstname&&lastname) {

                    mysqlConnection.query("UPDATE student SET firstname = '"+firstname+"', lastname = '"+lastname+"' WHERE rollno = "+rollno+"", (err, rows, fields)=> {
                        console.log(err);
        
                        if(!err) {
        
                            mysqlConnection.query("SELECT * FROM student WHERE rollno="+rollno+"", (err, rows, fields) => {

                                console.log(rows);

                                if(rows&&rows.length) {

                                    res.send(rows[0]);

                                }
                                else {

                                    console.log(err);
                                    res.send(err);
                                }
                            })
        
                        }
        
                        else {
        
                            res.send(err);
        
                        }
                    })
                }

                else if(firstname&&eng) {

                    mysqlConnection.query("UPDATE student SET firstname = '"+firstname+"', english = "+eng+" WHERE rollno = "+rollno+"", (err, rows, fields)=> {
                        console.log(err);
        
                        if(!err) {
        
                            mysqlConnection.query("SELECT * FROM student WHERE rollno="+rollno+"", (err, rows, fields) => {

                                console.log(rows);

                                if(rows&&rows.length) {

                                    res.send(rows[0]);

                                }
                                else {

                                    console.log(err);
                                    res.send(err);
                                }
                            })
        
                        }
        
                        else {
        
                            res.send(err);
        
                        }
                    })
                }

                else if(lastname&&eng) {

                    mysqlConnection.query("UPDATE student SET lastname = '"+lastname+"', english = "+eng+" WHERE rollno = "+rollno+"", (err, rows, fields)=> {
                        console.log(err);
        
                        if(!err) {
        
                            mysqlConnection.query("SELECT * FROM student WHERE rollno="+rollno+"", (err, rows, fields) => {

                                console.log(rows);

                                if(rows&&rows.length) {

                                    res.send(rows[0]);

                                }
                                else {

                                    console.log(err);
                                    res.send(err);
                                }
                            })
        
                        }
        
                        else {
        
                            res.send(err);
        
                        }
                    })
                }

                else if(firstname&&maths) {

                    mysqlConnection.query("UPDATE student SET firstname = '"+firstname+"', mathematics = "+maths+" WHERE rollno = "+rollno+"", (err, rows, fields)=> {
                        console.log(err);
        
                        if(!err) {
        
                            mysqlConnection.query("SELECT * FROM student WHERE rollno="+rollno+"", (err, rows, fields) => {

                                console.log(rows);

                                if(rows&&rows.length) {

                                    res.send(rows[0]);

                                }
                                else {

                                    console.log(err);
                                    res.send(err);
                                }
                            })
        
                        }
        
                        else {
        
                            res.send(err);
        
                        }
                    })
                }

                else if(lastname&&maths) {

                    mysqlConnection.query("UPDATE student SET lastname = '"+lastname+"', mathematics = "+maths+" WHERE rollno = "+rollno+"", (err, rows, fields)=> {
                        console.log(err);
        
                        if(!err) {
        
                            mysqlConnection.query("SELECT * FROM student WHERE rollno="+rollno+"", (err, rows, fields) => {

                                console.log(rows);

                                if(rows&&rows.length) {

                                    res.send(rows[0]);

                                }
                                else {

                                    console.log(err);
                                    res.send(err);
                                }
                            })
        
                        }
        
                        else {
        
                            res.send(err);
        
                        }
                    })
                }

                else if(eng&&maths) {

                    mysqlConnection.query("UPDATE student SET english = "+eng+", mathematics = "+maths+" WHERE rollno = "+rollno+"", (err, rows, fields)=> {
                        console.log(err);
        
                        if(!err) {
        
                            mysqlConnection.query("SELECT * FROM student WHERE rollno="+rollno+"", (err, rows, fields) => {

                                console.log(rows);

                                if(rows&&rows.length) {

                                    res.send(rows[0]);

                                }
                                else {

                                    console.log(err);
                                    res.send(err);
                                }
                            })
        
                        }
        
                        else {
        
                            res.send(err);
        
                        }
                    })
                }

                else if(firstname&&phy) {

                    mysqlConnection.query("UPDATE student SET firstname = '"+firstname+"', physics = "+phy+" WHERE rollno = "+rollno+"", (err, rows, fields)=> {
                        console.log(err);
        
                        if(!err) {
        
                            mysqlConnection.query("SELECT * FROM student WHERE rollno="+rollno+"", (err, rows, fields) => {

                                console.log(rows);

                                if(rows&&rows.length) {

                                    res.send(rows[0]);

                                }
                                else {

                                    console.log(err);
                                    res.send(err);
                                }
                            })
        
                        }
        
                        else {
        
                            res.send(err);
        
                        }
                    })
                }

                else if(lastname&&phy) {

                    mysqlConnection.query("UPDATE student SET lastname = '"+lastname+"', physics = "+phy+" WHERE rollno = "+rollno+"", (err, rows, fields)=> {
                        console.log(err);
        
                        if(!err) {
        
                            mysqlConnection.query("SELECT * FROM student WHERE rollno="+rollno+"", (err, rows, fields) => {

                                console.log(rows);

                                if(rows&&rows.length) {

                                    res.send(rows[0]);

                                }
                                else {

                                    console.log(err);
                                    res.send(err);
                                }
                            })
        
                        }
        
                        else {
        
                            res.send(err);
        
                        }
                    })
                }

                else if(eng&&phy) {

                    mysqlConnection.query("UPDATE student SET english = "+eng+", physics = "+phy+" WHERE rollno = "+rollno+"", (err, rows, fields)=> {
                        console.log(err);
        
                        if(!err) {
        
                            mysqlConnection.query("SELECT * FROM student WHERE rollno="+rollno+"", (err, rows, fields) => {

                                console.log(rows);

                                if(rows&&rows.length) {

                                    res.send(rows[0]);

                                }
                                else {

                                    console.log(err);
                                    res.send(err);
                                }
                            })
        
                        }
        
                        else {
        
                            res.send(err);
        
                        }
                    })
                }

                else if(maths&&phy) {

                    mysqlConnection.query("UPDATE student SET mathematics = "+maths+", physics = "+phy+" WHERE rollno = "+rollno+"", (err, rows, fields)=> {
                        console.log(err);
        
                        if(!err) {
        
                            mysqlConnection.query("SELECT * FROM student WHERE rollno="+rollno+"", (err, rows, fields) => {

                                console.log(rows);

                                if(rows&&rows.length) {

                                    res.send(rows[0]);

                                }
                                else {

                                    console.log(err);
                                    res.send(err);
                                }
                            })
        
                        }
        
                        else {
        
                            res.send(err);
        
                        }
                    })
                }

                else if(firstname&&che) {

                    mysqlConnection.query("UPDATE student SET firstname = '"+firstname+"', chemistry = "+che+" WHERE rollno = "+rollno+"", (err, rows, fields)=> {
                        console.log(err);
        
                        if(!err) {
        
                            mysqlConnection.query("SELECT * FROM student WHERE rollno="+rollno+"", (err, rows, fields) => {

                                console.log(rows);

                                if(rows&&rows.length) {

                                    res.send(rows[0]);

                                }
                                else {

                                    console.log(err);
                                    res.send(err);
                                }
                            })
        
                        }
        
                        else {
        
                            res.send(err);
        
                        }
                    })
                }

                else if(lastname&&che) {

                    mysqlConnection.query("UPDATE student SET lastname = '"+lastname+"', chemistry = "+che+" WHERE rollno = "+rollno+"", (err, rows, fields)=> {
                        console.log(err);
        
                        if(!err) {
        
                            mysqlConnection.query("SELECT * FROM student WHERE rollno="+rollno+"", (err, rows, fields) => {

                                console.log(rows);

                                if(rows&&rows.length) {

                                    res.send(rows[0]);

                                }
                                else {

                                    console.log(err);
                                    res.send(err);
                                }
                            })
        
                        }
        
                        else {
        
                            res.send(err);
        
                        }
                    })
                }

                if(eng&&che) {

                    mysqlConnection.query("UPDATE student SET english = "+eng+", chemistry = "+che+" WHERE rollno = "+rollno+"", (err, rows, fields)=> {
                        console.log(err);
        
                        if(!err) {
        
                            mysqlConnection.query("SELECT * FROM student WHERE rollno="+rollno+"", (err, rows, fields) => {

                                console.log(rows);

                                if(rows&&rows.length) {

                                    res.send(rows[0]);

                                }
                                else {

                                    console.log(err);
                                    res.send(err);
                                }
                            })
        
                        }
        
                        else {
        
                            res.send(err);
        
                        }
                    })
                }

                else if(maths&&che) {

                    mysqlConnection.query("UPDATE student SET mathematics = "+maths+", chemistry = "+che+" WHERE rollno = "+rollno+"", (err, rows, fields)=> {
                        console.log(err);
        
                        if(!err) {
        
                            mysqlConnection.query("SELECT * FROM student WHERE rollno="+rollno+"", (err, rows, fields) => {

                                console.log(rows);

                                if(rows&&rows.length) {

                                    res.send(rows[0]);

                                }
                                else {

                                    console.log(err);
                                    res.send(err);
                                }
                            })
        
                        }
        
                        else {
        
                            res.send(err);
        
                        }
                    })
                }

                else if(phy&&che) {

                    mysqlConnection.query("UPDATE student SET physics = "+phy+", chemistry = "+che+" WHERE rollno = "+rollno+"", (err, rows, fields)=> {
                        console.log(err);
        
                        if(!err) {
        
                            mysqlConnection.query("SELECT * FROM student WHERE rollno="+rollno+"", (err, rows, fields) => {

                                console.log(rows);

                                if(rows&&rows.length) {

                                    res.send(rows[0]);

                                }
                                else {

                                    console.log(err);
                                    res.send(err);
                                }
                            })
        
                        }
        
                        else {
        
                            res.send(err);
        
                        }
                    })
                }

                else if(firstname&&bio) {

                    mysqlConnection.query("UPDATE student SET firstname = '"+firstname+"', biology = "+bio+" WHERE rollno = "+rollno+"", (err, rows, fields)=> {
                        console.log(err);
        
                        if(!err) {
        
                            mysqlConnection.query("SELECT * FROM student WHERE rollno="+rollno+"", (err, rows, fields) => {

                                console.log(rows);

                                if(rows&&rows.length) {

                                    res.send(rows[0]);

                                }
                                else {

                                    console.log(err);
                                    res.send(err);
                                }
                            })
        
                        }
        
                        else {
        
                            res.send(err);
        
                        }
                    })
                }

                else if(lastname&&bio) {

                    mysqlConnection.query("UPDATE student SET lastname = '"+lastname+"', biology = "+bio+" WHERE rollno = "+rollno+"", (err, rows, fields)=> {
                        console.log(err);
        
                        if(!err) {
        
                            mysqlConnection.query("SELECT * FROM student WHERE rollno="+rollno+"", (err, rows, fields) => {

                                console.log(rows);

                                if(rows&&rows.length) {

                                    res.send(rows[0]);

                                }
                                else {

                                    console.log(err);
                                    res.send(err);
                                }
                            })
        
                        }
        
                        else {
        
                            res.send(err);
        
                        }
                    })
                }

                else if(eng&&bio) {

                    mysqlConnection.query("UPDATE student SET english = "+eng+", biology = "+bio+" WHERE rollno = "+rollno+"", (err, rows, fields)=> {
                        console.log(err);
        
                        if(!err) {
        
                            mysqlConnection.query("SELECT * FROM student WHERE rollno="+rollno+"", (err, rows, fields) => {

                                console.log(rows);

                                if(rows&&rows.length) {

                                    res.send(rows[0]);

                                }
                                else {

                                    console.log(err);
                                    res.send(err);
                                }
                            })
        
                        }
        
                        else {
        
                            res.send(err);
        
                        }
                    })
                }

                else if(maths&&bio) {

                    mysqlConnection.query("UPDATE student SET mathematics = "+maths+", biology = "+bio+" WHERE rollno = "+rollno+"", (err, rows, fields)=> {
                        console.log(err);
        
                        if(!err) {
        
                            mysqlConnection.query("SELECT * FROM student WHERE rollno="+rollno+"", (err, rows, fields) => {

                                console.log(rows);

                                if(rows&&rows.length) {

                                    res.send(rows[0]);

                                }
                                else {

                                    console.log(err);
                                    res.send(err);
                                }
                            })
        
                        }
        
                        else {
        
                            res.send(err);
        
                        }
                    })
                }

                else if(phy&&bio) {

                    mysqlConnection.query("UPDATE student SET physics = "+phy+", biology = "+bio+" WHERE rollno = "+rollno+"", (err, rows, fields)=> {
                        console.log(err);
        
                        if(!err) {
        
                            mysqlConnection.query("SELECT * FROM student WHERE rollno="+rollno+"", (err, rows, fields) => {

                                console.log(rows);

                                if(rows&&rows.length) {

                                    res.send(rows[0]);

                                }
                                else {

                                    console.log(err);
                                    res.send(err);
                                }
                            })
        
                        }
        
                        else {
        
                            res.send(err);
        
                        }
                    })
                }

                else if(che&&bio) {

                    mysqlConnection.query("UPDATE student SET chemistry = "+che+", biology = "+bio+" WHERE rollno = "+rollno+"", (err, rows, fields)=> {
                        console.log(err);
        
                        if(!err) {
        
                            mysqlConnection.query("SELECT * FROM student WHERE rollno="+rollno+"", (err, rows, fields) => {

                                console.log(rows);

                                if(rows&&rows.length) {

                                    res.send(rows[0]);

                                }
                                else {

                                    console.log(err);
                                    res.send(err);
                                }
                            })
        
                        }
        
                        else {
        
                            res.send(err);
        
                        }
                    })
                }

                else if(firstname) {

                    mysqlConnection.query("UPDATE student SET firstname = '"+firstname+" WHERE rollno = "+rollno+"", (err, rows, fields)=> {
                        console.log(err);
                        if(!err) {
    
                            mysqlConnection.query("SELECT * FROM student WHERE rollno="+rollno+"", (err, rows, fields) => {

                                console.log(rows);

                                if(rows&&rows.length) {

                                    res.send(rows[0]);

                                }
                                else {

                                    console.log(err);
                                    res.send(err);
                                }
                            })
        
                        }
        
                        else {
        
                            res.send(err);
        
                        }
                    })
                }
                else if(lastname) {

                    mysqlConnection.query("UPDATE student SET lastname = '"+lastname+" WHERE rollno = "+rollno+"", (err, rows, fields)=> {
                        console.log(err);
                        if(!err) {
    
                            mysqlConnection.query("SELECT * FROM student WHERE rollno="+rollno+"", (err, rows, fields) => {

                                console.log(rows);

                                if(rows&&rows.length) {

                                    res.send(rows[0]);

                                }
                                else {

                                    console.log(err);
                                    res.send(err);
                                }
                            })
        
                        }
        
                        else {
        
                            res.send(err);
        
                        }
                    })
                }
                else if(eng) {

                    mysqlConnection.query("UPDATE student SET english = "+eng+" WHERE rollno = "+rollno+"", (err, rows, fields)=> {
                        console.log(err);
                        if(!err) {
    
                            mysqlConnection.query("SELECT * FROM student WHERE rollno="+rollno+"", (err, rows, fields) => {

                                console.log(rows);

                                if(rows&&rows.length) {

                                    res.send(rows[0]);

                                }
                                else {

                                    console.log(err);
                                    res.send(err);
                                }
                            })
        
                        }
        
                        else {
        
                            res.send(err);
        
                        }
                    })

                }
                else if(maths) {

                    mysqlConnection.query("UPDATE student SET mathematics = "+maths+" WHERE rollno = "+rollno+"", (err, rows, fields)=> {
                        console.log(err);
                        if(!err) {
    
                            mysqlConnection.query("SELECT * FROM student WHERE rollno="+rollno+"", (err, rows, fields) => {

                                console.log(rows);

                                if(rows&&rows.length) {

                                    res.send(rows[0]);

                                }
                                else {

                                    console.log(err);
                                    res.send(err);
                                }
                            })
        
                        }
        
                        else {
        
                            res.send(err);
        
                        }
                    })

                }
                else if(phy) {

                    mysqlConnection.query("UPDATE student SET physics = "+phy+" WHERE rollno = "+rollno+"", (err, rows, fields)=> {
                        console.log(err);
                        if(!err) {
    
                            mysqlConnection.query("SELECT * FROM student WHERE rollno="+rollno+"", (err, rows, fields) => {

                                console.log(rows);

                                if(rows&&rows.length) {

                                    res.send(rows[0]);

                                }
                                else {

                                    console.log(err);
                                    res.send(err);
                                }
                            })
        
                        }
        
                        else {
        
                            res.send(err);
        
                        }
                    })

                }
                else if(che) {

                    mysqlConnection.query("UPDATE student SET chemistry = "+che+" WHERE rollno = "+rollno+"", (err, rows, fields)=> {
                        console.log(err);
                        if(!err) {
    
                            mysqlConnection.query("SELECT * FROM student WHERE rollno="+rollno+"", (err, rows, fields) => {

                                console.log(rows);

                                if(rows&&rows.length) {

                                    res.send(rows[0]);

                                }
                                else {

                                    console.log(err);
                                    res.send(err);
                                }
                            })
        
                        }
        
                        else {
        
                            res.send(err);
        
                        }
                    })

                }
                else if(bio) {

                    mysqlConnection.query("UPDATE student SET biology = "+bio+" WHERE rollno = "+rollno+"", (err, rows, fields)=> {
                        console.log(err);
                        if(!err) {
    
                            mysqlConnection.query("SELECT * FROM student WHERE rollno="+rollno+"", (err, rows, fields) => {

                                console.log(rows);

                                if(rows&&rows.length) {

                                    res.send(rows[0]);

                                }
                                else {

                                    console.log(err);
                                    res.send(err);
                                }
                            })
        
                        }
        
                        else {
        
                            res.send(err);
        
                        }
                    })

                }
                else {

                    console.log("You must enter at least one field to update!")
                    res.send("You must enter at least one field to update!");

                }
                                                    
            }
            else {
                
                console.log("No record exists with the given Rollno!")
                res.send("No record exists with the given Rollno!");

            }
        })
    }

    else if(rollno==0) {
        
        console.log("Rollno can't be 0!");
        res.send("Rollno can't be 0!");
    }
    
    else {

        console.log("Invalid Rollno!");
        res.send("Invalid Rollno!");
    }

})

Router.delete("/:rollno", (req, res)=> {

    var rollno = parseInt(req.params.rollno);

    if(rollno&&rollno!=0) {

        mysqlConnection.query("SELECT * FROM student WHERE rollno = "+rollno+"", (err, rows, fields)=> {
            console.log(rows);
            if(rows&&rows.length) {
                
                mysqlConnection.query("DELETE FROM student WHERE rollno = "+rollno+"", (err, rows, fields)=> {
                    console.log(err);
                                
                    if(!err) {
        
                        res.send("Record deleted!");
        
                    }
        
                    else {
        
                        res.send(err);
        
                    }
                })
    
            }                
            
            else {
                
                res.send("No record exists with the given Rollno!");
    
            }
        })
    }

    else if(rollno==0) {

        console.log("Rollno can't be 0!");
        res.send("Rollno can't be 0!");
    }

    else {
        
        console.log("Invalid Rollno!");
        res.send("Invalid Rollno!");

    }
})

Router.get("/", (req,res)=> {

    mysqlConnection.query("SELECT rollno, firstname, lastname FROM student", (err, rows, fields)=> {
        if(!err) {
             
            console.log(rows);
            res.send(rows);
                    
        }
        else {
            
            console.log(err);

        }
    })
   
})


Router.get("/:rollno", (req,res)=> {

    var rollno = parseInt(req.params.rollno);

    if(rollno&&rollno!=0) {

        mysqlConnection.query("SELECT *, (english + mathematics + physics + chemistry + biology) AS totalMarks, (english + mathematics + physics + chemistry + biology)/5 AS averageMarks FROM student WHERE rollno = "+rollno+"", (err, rows, fields)=> {
            if(rollno&&rows&&rows.length) {
                 
                console.log(rows);
                res.send(rows[0]);
                        
            }
            else {
                console.log("No record exists with the given Rollno!");
                res.send("No record exists with the given Rollno!");
            }
        })
    }

    else if(rollno==0) {
    
        console.log("Rollno can't be 0!");
        res.send("Rollno can't be 0!");
    }

    else {
    
        console.log("Invalid Rollno!");
        res.send("Invalid Rollno!");

    }

})

Router.get("/:rollno/result", (req,res)=> {

    var rollno = parseInt(req.params.rollno);

    if(rollno&&rollno!=0) {

        mysqlConnection.query("SELECT *, (english + mathematics + physics + chemistry + biology) AS totalMarks, (english + mathematics + physics + chemistry + biology)/5 AS averageMarks, CASE WHEN (((english + mathematics + physics + chemistry + biology)/500)*100)>=80 THEN 'PASSED WITH DISTINCTION' WHEN (((english + mathematics + physics + chemistry + biology)/500)*100)>=60 THEN 'PASSED WITH FIRST CLASS' WHEN (((english + mathematics + physics + chemistry + biology)/500)*100)>=50 THEN 'PASSED WITH SECOND CLASS' WHEN (((english + mathematics + physics + chemistry + biology)/500)*100)>=40 THEN 'PASSED WITH THIRD CLASS' ELSE 'FAILED' END AS result FROM student WHERE rollno = "+rollno+"", (err, rows, fields)=> {
            if(rollno&&rows&&rows.length) {
                

                console.log(rows);
                res.send(rows[0]);
                        
            }
            else {
                console.log("No record exists with the given Rollno!");
                res.send("No record exists with the given Rollno!");
            }
        })
    }

    else if(rollno==0) {
    
        console.log("Rollno can't be 0!");
        res.send("Rollno can't be 0!");
    }

    else {
    
        console.log("Invalid Rollno!");
        res.send("Invalid Rollno!");
    }

})

module.exports = Router;