var mysql = require("mysql");

var mysqlConnection = mysql.createConnection({
    host : "localhost",
    user : "root",
    password : "",
    database : "classroom",
    multipleStatements : true
});

mysqlConnection.connect((err)=> {
    if(!err) {
        console.log("Connected");
    }
    else {
        console.log("Connection failed", err);
    }
});

module.exports = mysqlConnection;