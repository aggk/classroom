var express = require("express");
var bodyParser = require("body-parser");
var mysqlConnection = require("./connection");
var studentRoutes = require("./routes/student");
var classroomRoutes = require("./routes/classroom");

var app = express();

app.use(bodyParser.json());

app.use("/students", studentRoutes);
app.use("/classroom", classroomRoutes);



app.listen(8000);